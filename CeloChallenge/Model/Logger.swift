//
//  Logger.swift
//  CeloChallenge
//
//  Created by Javid Noutash on 3/07/20.
//  Copyright © 2020 Noutash. All rights reserved.
//
import Foundation

/**
 A simple struct to write well formatted and detailed logs
 */

struct Logger {
    static func log(_ message: Any, file: String = #file, function: String = #function) {
        #if DEBUG
        print("\(Date()) \((file as NSString).lastPathComponent) - \(function) INFO: \(message)")
        #endif
    }
    
    static func warn(_ message: Any, file: String = #file, function: String = #function) {
        #if DEBUG
        print("\(Date()) \((file as NSString).lastPathComponent) - \(function) WARNING: \(message)")
        #endif
    }
    
    static func error(_ message: Any, file: String = #file, function: String = #function) {
        #if DEBUG
        debugPrint("\(Date()) \((file as NSString).lastPathComponent) - \(function) ERROR: \(message)")
        #endif
    }
}
