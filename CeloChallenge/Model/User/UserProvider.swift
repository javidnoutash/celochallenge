//
//  UserProvider.swift
//  CeloChallenge
//
//  Created by Javid Noutash on 3/07/20.
//  Copyright © 2020 Noutash. All rights reserved.
//

import Foundation
import CoreData

/**
 A provider class that fetches the user data from API and saves them to the Core Data store
 */
class UserProvider {
    /**
     Constants
     */
    let seed = "celochallenge"
    let totalUsers = 1000
    let usersToSavePerBatch = 20
    var url: String {
        return "https://randomuser.me/api/?seed=\(seed)&results=\(totalUsers)"
    }
    
    /**
     Variables
     */
    var isFetching: Bool = false
    
    /**
     A persistent container to set up the Core Data stack.
     */
    lazy var persistentContainer: NSPersistentContainer = {
        let container = NSPersistentContainer(name: "CeloChallenge")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        
        // In this app, we refresh the UI by refetching data, so no need to merge the changes.
        container.viewContext.automaticallyMergesChangesFromParent = false
        return container
    }()
    
    /**
     Creates and configures a private queue context.
     */
    private func newTaskContext() -> NSManagedObjectContext {
        let taskContext = persistentContainer.newBackgroundContext()
        taskContext.mergePolicy = NSMergeByPropertyObjectTrumpMergePolicy
        return taskContext
    }
    
    /**
     Fetches the users data from the remote server and imports them into Core Data
     */
    func fetch(batchSavedHandler: @escaping (Int) -> Void, completionHandler: @escaping (Error?) -> Void) {
        if isFetching {
            Logger.log("Fetching process is already running")
            completionHandler(nil)
            return
        } else {
            isFetching = true
        }
        
        Logger.log("Fetching \(totalUsers) users from server...")
        
        guard let url = URL(string: url) else {
            Logger.error(CustomError.urlError.localizedDescription)
            completionHandler(CustomError.urlError)
            self.isFetching = false
            return
        }
        
        // Create a URLSession and a dataTask to fetch the users.
        let session = URLSession(configuration: .default)
        let task = session.dataTask(with: url) { data, _, urlSessionError in
            
            guard urlSessionError == nil else {
                Logger.error(urlSessionError!.localizedDescription)
                completionHandler(urlSessionError)
                self.isFetching = false
                return
            }
            
            guard let data = data else {
                Logger.error(CustomError.networkUnavailable.localizedDescription)
                completionHandler(CustomError.networkUnavailable)
                self.isFetching = false
                return
            }
            
            do {
                // Decode the JSON into Decodable type UsersResponse.
                let usersResponse = try JSONDecoder().decode(UsersResponse.self, from: data)
                
                // Delete the existing data in case some records are deleted in the server
                try self.deleteAll()
                
                // Save records in batches to avoid a high memory footprint.
                let numUsers = usersResponse.results.count
                let batchSize = self.usersToSavePerBatch
                
                // Calculate the total number of batches
                var numBatches = numUsers / batchSize
                numBatches += numUsers % batchSize > 0 ? 1 : 0
                
                Logger.log("Got \(numUsers) users, saving them in \(numBatches) batches of ~\(batchSize)")

                for batch in 0 ..< numBatches {
                    // Calculate the range for this batch.
                    let batchStart = batch * batchSize
                    let batchEnd = batchStart + min(batchSize, numUsers - batch * batchSize)
                    let range = batchStart..<batchEnd
                    
                    // Save the current batch of the fetched users
                    // Stop saving if any batch is unsuccessful
                    try self.save(Array(usersResponse.results[range]))
                    Logger.log("Progress: \((batch + 1) * 100 / numBatches)%")
                    
                    // Notify the UI when a batch is saved
                    batchSavedHandler(batch)
                }

            } catch {
                // Error while parsing the JSON or saving the data
                completionHandler(error)
                self.isFetching = false
                Logger.error(error.localizedDescription)
                return
            }
            
            completionHandler(nil)
            self.isFetching = false
            Logger.log("Finished fetching all the users.")
        }
                    
        // Start the task.
        task.resume()
    }
    
    /**
     A function to create User entities from UserDto and store them in the Core Data store
     */
    func save(_ usersBatch: [UserDto]) throws {
        let taskContext = newTaskContext()
        var performError: Error?
        
        // taskContext.performAndWait runs on the URLSession's delegate queue
        // so it won’t block the main thread.
        taskContext.performAndWait {
            // Create a new record for each user in the batch.
            for userDto in usersBatch {
                // Create a User managed object on the private queue context.
                guard let user = NSEntityDescription.insertNewObject(forEntityName: "User", into: taskContext) as? User else {
                    performError = CustomError.creationError
                    Logger.error(performError!.localizedDescription)
                    return
                }
                
                // Populate the User's properties using the UserDto data.
                user.update(with: userDto)
            }
            
            // Save all insertions and deletions from the context to the store.
            if taskContext.hasChanges {
                do {
                    try taskContext.save()
                } catch {
                    performError = error
                    return
                }
                // Reset the taskContext to free the cache and lower the memory footprint.
                taskContext.reset()
            }
        }
        
        if let error = performError {
            throw error
        }
    }
    
    /**
     Deletes all the User records in the Core Data store.
    */
    func deleteAll() throws {
        let taskContext = newTaskContext()
        var performError: Error?

        taskContext.performAndWait {
            let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "User")
            let batchDeleteRequest = NSBatchDeleteRequest(fetchRequest: fetchRequest)
            batchDeleteRequest.resultType = .resultTypeCount
            
            // Execute the batch delete
            let batchDeleteResult = try? taskContext.execute(batchDeleteRequest) as? NSBatchDeleteResult
            if batchDeleteResult == nil || batchDeleteResult?.result == nil {
                performError = CustomError.batchDeleteError
            }
        }
        
        if let error = performError {
            throw error
        }
    }
    
    /**
     Loads the Users from Core Data store based on the passed arguments
     */
    func load(limit: Int = 10, offset: Int = 0, filter: NSString? = nil, completion: @escaping ([User]?, Error?) -> Void) {
        // run the loading task in background for a smoother user experience
        DispatchQueue.global(qos: .background).async {
            let fetchRequest = NSFetchRequest<User>(entityName: "User")
            
            if let filter = filter {
                let words = filter.trimmingCharacters(in: .whitespaces).components(separatedBy: .whitespaces)
                let predicates = words.map { (word) -> NSPredicate in
                    NSPredicate(format: "firstName CONTAINS[cd] %@ OR lastName CONTAINS[cd] %@", word, word)
                }
                
                fetchRequest.predicate = NSCompoundPredicate(type: .and, subpredicates: predicates)
            } else {
                fetchRequest.fetchLimit = limit
                fetchRequest.fetchOffset = offset
            }
            
            do {
                let users = try self.persistentContainer.viewContext.fetch(fetchRequest)
                completion(users, nil)
            } catch {
                completion(nil, error)
            }
        }
    }
}
