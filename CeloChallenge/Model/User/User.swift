//
//  User.swift
//  CeloChallenge
//
//  Created by Javid Noutash on 3/07/20.
//  Copyright © 2020 Noutash. All rights reserved.
//
import Foundation

/**
Managed object subclass extension for the User entity.
*/
extension User {
    /**
     Updates a User instance with data provided in a UserDto
     */
    func update(with userDto: UserDto?) {
        if let userData = userDto {
            self.title = userData.name.title
            self.firstName = userData.name.first
            self.lastName = userData.name.last
            self.gender = userData.gender
            self.email = userData.email
            self.phone = userData.phone
            self.dateOfBirth = Date.fromString(userData.dob.date, withFormat: "yyyy-MM-dd'T'HH:mm:ss.SSSZ")
            self.latitude = Double(userData.location.coordinates.latitude) ?? 0.0
            self.longitude = Double(userData.location.coordinates.longitude) ?? 0.0

            if let imageUrl = URL(string: userData.picture.large) {
                do {
                    self.imageData = try Data(contentsOf: imageUrl)
                } catch {
                    Logger.warn(error.localizedDescription)
                }
            }
        }
    }
}

// MARK - Decodable

/**
 A struct for decoding JSON with the following structure:
 
 {
   "results": [
     {
       "gender": "male",
       "name": {
         "title": "Mr",
         "first": "Alexis",
         "last": "Lemaire"
       },
       "location": {
         "coordinates": {
           "latitude": "-2.9665",
           "longitude": "-39.7420"
         },
        ...
       },
       "email": "alexis.lemaire@example.com",
       "dob": {
         "date": "1976-08-04T01:19:16.879Z",
         ...
       },
       "phone": "02-30-80-71-67",
       "picture": {
         "large": "https:\//randomuser.me/api/portraits/men/8.jpg",
         ...
       },
       ...
     }
   ],
   ...
 }
 */

struct UsersResponse: Decodable {
    var results: [UserDto]
}

struct UserDto: Decodable {
    var gender: String
    var email: String
    var phone: String
    var name: UserNameDto
    var dob: UserDateOfBirthDto
    var location: UserLocationDto
    var picture: UserPictureDto
}

struct UserNameDto: Decodable {
    var title: String
    var first: String
    var last: String
}

struct UserDateOfBirthDto: Decodable {
    var date: String
}

struct UserLocationDto: Decodable {
    var coordinates: UserLocationCoordinatesDto
}

struct UserLocationCoordinatesDto: Decodable {
    var latitude: String
    var longitude: String
}

struct UserPictureDto: Decodable {
    var large: String
}
