//
//  UsersListVC.swift
//  CeloChallenge
//
//  Created by Javid Noutash on 3/07/20.
//  Copyright © 2020 Noutash. All rights reserved.
//

import UIKit

class UsersListVC: UIViewController {
    
    // Variables
    let userProvider: UserProvider = UserProvider()
    var users: [User] = []
    var page: Int = 0
    var usersPerPage: Int = 10
    var refreshControl: UIRefreshControl = UIRefreshControl()
    
    // Outlets
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.refreshControl = refreshControl
        refreshControl.addTarget(self, action: #selector(fetchUsers), for: .valueChanged)
        refreshControl.beginRefreshing()
        
        fetchUsers()
    }
    
    @objc func fetchUsers() {
        // Fetch users from the remote server and save them to the Core Data store
        userProvider.fetch(batchSavedHandler: { (batch) in
            // Load users as soon as the first batch of users is saved
            if (batch == 0) {
                self.userProvider.load() { (users, error) in
                    self.handleUsersLoad(with: users, and: error)
                }
            }
        }) { (error) in
            if let error = error {
                Logger.error(error.localizedDescription)
                
                // load data from Core Data store if not connected to the Internet
                if (error as NSError).code == NSURLErrorNotConnectedToInternet {
                    self.userProvider.load() { (users, error) in
                        self.handleUsersLoad(with: users, and: error)
                    }
                } else {
                    self.showSimpleAlert(message: error.localizedDescription)
                }
            } else {
                self.stopRefresh()
            }
        }
    }
    
    func handleUsersLoad(with users: [User]?, and error: Error?, append: Bool = false) {
        stopRefresh()
        
        if let error = error {
            Logger.error(error.localizedDescription)
            showSimpleAlert(message: error.localizedDescription)
        } else if let users = users {
            if append {
                self.users += users
            } else {
                self.users = users
            }
            
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }
    }
    
    /**
     Shows a simple alert dialog with a predefined Ok button
     */
    func showSimpleAlert(message: String) {
        DispatchQueue.main.async {
            let alert = UIAlertController(title: "Oops!", message: message, preferredStyle: .alert)
            let ok = UIAlertAction(title: "Ok", style: .default)
            alert.addAction(ok)
            
            self.present(alert, animated: true) {
                self.stopRefresh()
            }
        }
    }
    
    /**
     Hides the refresh indicator if it is visible
     */
    func stopRefresh() {
        DispatchQueue.main.async {
            if self.refreshControl.isRefreshing {
                self.refreshControl.endRefreshing()
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == Constants.Storyboard.Segues.toUserDetails {
            let userDetailsVC = segue.destination as! UserDetailsVC
            if let user = sender as? User {
                userDetailsVC.user = user
            }
        }
    }
}

// MARK - UITableViewDataSource
extension UsersListVC: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return users.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: Constants.Storyboard.Cells.user, for: indexPath) as? UserListCell else {
            Logger.error("\(Constants.Storyboard.Cells.user) doesn't return a reusable cell")
            return UserListCell()
        }
        
        cell.configure(with: users[indexPath.row])
        return cell
    }
}

// MARK - UITableViewDelegate
extension UsersListVC: UITableViewDelegate {
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        // Load more users when the last cell of the list is going to render if user is not searching
        if indexPath.row == users.count - 1 && searchBar.text?.count == 0 {
            page += 1
            userProvider.load(limit: usersPerPage, offset: page * usersPerPage) { (users, error) in
                self.handleUsersLoad(with: users, and: error, append: true)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: Constants.Storyboard.Segues.toUserDetails, sender: users[indexPath.row])
    }
}

// MARK - UISearchBarDelegate
extension UsersListVC: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        // reset the list if the SearchBar is cleared
        if searchBar.text?.count == 0 {
            userProvider.load() { (users, error) in
                self.handleUsersLoad(with: users, and: error)
            }
        }
    }
    
    // dismiss the keyboard when the Search button is tapped
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        if let searchedText = searchBar.text, searchedText.count > 0 {
            userProvider.load(filter: searchedText as NSString?) { (users, error) in
                self.handleUsersLoad(with: users, and: error)
            }
        }
    }
    
    // dismiss the keyboard when the Cancel button is tapped
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
}
