//
//  UserDetailsVC.swift
//  CeloChallenge
//
//  Created by Javid Noutash on 3/07/20.
//  Copyright © 2020 Noutash. All rights reserved.
//

import UIKit
import MapKit

class UserDetailsVC: UIViewController {

    // Variables
    var user: User!
    
    // Outlets
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var imageViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var genderLabel: UILabel!
    @IBOutlet weak var dobLabel: UILabel!
    @IBOutlet weak var phoneLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var locationMapView: MKMapView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configure()
    }
    
    func configure() {
        if let imageData = user.imageData {
            imageView.image = UIImage(data: imageData)
            imageView.round(borderRadius: 5, borderColor: UIColor.darkGray, borderWidth: 3.0)
        }
        
        nameLabel.text = "\(user.title ?? ""). \(user.firstName ?? "") \(user.lastName ?? "")"
        genderLabel.text = user.gender?.capitalized
        phoneLabel.text = user.phone
        emailLabel.text = user.email
        
        if let dob = user.dateOfBirth {
            dobLabel.text = dob.toString(format: "MMMM dd, yyyy")
        } else {
            dobLabel.text = "-"
        }
        
        let annotation = MKPointAnnotation()
        annotation.title = "\(user.firstName ?? "") \(user.lastName ?? "")"
        annotation.coordinate = CLLocationCoordinate2DMake(user.latitude, user.longitude)
        
        let region = MKCoordinateRegion(center: annotation.coordinate, latitudinalMeters: 10000, longitudinalMeters: 10000)
        
        locationMapView.addAnnotation(annotation)
        locationMapView.setRegion(region, animated: true)
    }
}
