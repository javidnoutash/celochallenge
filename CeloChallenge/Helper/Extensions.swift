//
//  Extensions.swift
//  CeloChallenge
//
//  Created by Javid Noutash on 3/07/20.
//  Copyright © 2020 Noutash. All rights reserved.
//

import Foundation
import UIKit

/**
 Extension subclasses of objects or data types to extend their funtionalities
 */

// MARK - Date
extension Date {
    /**
     A helper function to take a string with its date format and convert it to a potential date
     */
    static func fromString(_ dateString: String, withFormat format: String) -> Date? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        return dateFormatter.date(from: dateString)
    }
    
    /**
     A helper function that converts a date to a string with the given format
     */
    func toString(format: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        return dateFormatter.string(from: self)
    }
}

// MARK - UIIMageView
extension UIImageView {
    /**
     A helper function to convert a square shape ImageView to a circle
     */
    func round() {
        self.layer.masksToBounds = false
        self.layer.cornerRadius = self.frame.height / 2
        self.clipsToBounds = true
    }
    
    /**
     A helper function to add border radius to an ImageView
     */
    func round(borderRadius: CGFloat, borderColor: UIColor, borderWidth: CGFloat) {
        self.layer.masksToBounds = false
        self.layer.cornerRadius = borderRadius
        self.layer.borderColor = borderColor.cgColor
        self.layer.borderWidth = borderWidth
        self.clipsToBounds = true
    }
}
