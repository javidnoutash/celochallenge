//
//  Constants.swift
//  CeloChallenge
//
//  Created by Javid Noutash on 3/07/20.
//  Copyright © 2020 Noutash. All rights reserved.
//

struct Constants {
    struct Storyboard {
        struct Cells {
            static let user = "UserListCell"
        }
        struct Segues {
            static let toUserDetails = "toUserDetailsVC"
        }
    }
}
