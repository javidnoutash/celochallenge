//
//  UserListCell.swift
//  CeloChallenge
//
//  Created by Javid Noutash on 3/07/20.
//  Copyright © 2020 Noutash. All rights reserved.
//

import UIKit

class UserListCell: UITableViewCell {

    @IBOutlet weak var pictureImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var dobLabel: UILabel!
    @IBOutlet weak var genderLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        pictureImageView.round()
    }
    // set the cell view data from a User object
    func configure(with user: User) {
        nameLabel.text = "\(user.title ?? ""). \(user.firstName ?? "") \(user.lastName ?? "")"
        genderLabel.text = user.gender?.capitalized
        
        if let dob = user.dateOfBirth {
            dobLabel.text = dob.toString(format: "MMM dd, yyy")
        } else {
            dobLabel.text = "-"
        }
        
        if let imageData = user.imageData {
            pictureImageView.image = UIImage(data: imageData)
        }
    }
}
